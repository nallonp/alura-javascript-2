const campos = [
    document.querySelector('#data'),
    document.querySelector('#quantidade'),
    document.querySelector('#valor')
];
document.querySelector('.form').addEventListener('submit', function (event) {
    event.preventDefault();
    const form = document.querySelector('.form');
    const tr = document.createElement('tr');
    const tbody = document.querySelector('table tbody');
    campos.forEach(function (campo) {
        const td = document.createElement('td');
        td.textContent = campo.value;
        tr.appendChild(td);
    })
    const tdVolume = document.createElement('td');
    tdVolume.textContent = campos[1].value * campos[2].value;
    tr.appendChild(tdVolume);
    tbody.appendChild(tr);
    form.reset();
});